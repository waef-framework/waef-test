use std::{sync::mpsc::Sender, time::Duration, vec};

use waef_core::{
    actors::{ActorAlignment, ActorsPlugin, HasActorAlignment, IsActor, UseActors},
    core::{ExecutionResult, IsDispatcher, IsDisposable, IsTickable, Message, WaefError},
    timers::IsTimer,
};

pub enum TestScenarioResult {
    Continue,
    Success,
    Failed(WaefError),
}

pub trait TestScenario {
    fn name(&self) -> String;

    fn setup(&mut self, _dispatch: Sender<Message>) {}
    fn cleanup(&mut self, _dispatch: Sender<Message>) {}

    fn step(&mut self, _dispatch: Sender<Message>) -> TestScenarioResult {
        TestScenarioResult::Continue
    }
    fn receive(&mut self, _msg: &Message, _dispatch: Sender<Message>) -> TestScenarioResult {
        TestScenarioResult::Continue
    }
}

pub struct TestsPlugin {
    current_test_index: Option<usize>,
    scenarios: Vec<Box<dyn TestScenario + Send>>,
    dispatch: Sender<Message>,
}
impl TestsPlugin {
    pub fn new(dispatch: Sender<Message>) -> Self {
        Self {
            dispatch,
            scenarios: vec![],
            current_test_index: None,
        }
    }

    pub fn with_scenario(mut self, scenario: impl TestScenario + 'static + Send) -> Self {
        self.scenarios.push(Box::new(scenario));
        self
    }
}
impl IsDispatcher for TestsPlugin {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        match self.current_test_index {
            None => {
                println!("Starting up WAEF Integration Testing Framework");
                if !self.scenarios.is_empty() {
                    println!("Starting test {}", self.scenarios[0].name());
                    self.scenarios[0].setup(self.dispatch.clone());

                    self.current_test_index = Some(0);
                    ExecutionResult::Processed
                } else {
                    println!("No test scenarios registered!");
                    ExecutionResult::Kill
                }
            }
            Some(test_index) => {
                if test_index >= self.scenarios.len() {
                    println!("All tests completed.");
                    ExecutionResult::Kill
                } else {
                    let result = if message.name == "integration_tests:advance" {
                        self.scenarios[test_index].step(self.dispatch.clone())
                    } else {
                        self.scenarios[test_index].receive(message, self.dispatch.clone())
                    };
                    match result {
                        TestScenarioResult::Continue => {
                            print!(".");
                            ExecutionResult::Processed
                        }
                        TestScenarioResult::Success => {
                            self.scenarios[test_index].cleanup(self.dispatch.clone());
                            println!("Test {} passed!", self.scenarios[test_index].name());

                            let next_index = test_index + 1;
                            self.current_test_index = Some(next_index);
                            if next_index < self.scenarios.len() {
                                println!("Starting test {}", self.scenarios[next_index].name());
                                self.scenarios[next_index].setup(self.dispatch.clone());
                            }
                            ExecutionResult::Processed
                        }
                        TestScenarioResult::Failed(reason) => {
                            self.scenarios[test_index].cleanup(self.dispatch.clone());
                            println!(
                                "Test {} failed!: {:?}",
                                self.scenarios[test_index].name(),
                                reason
                            );

                            let next_index = test_index + 1;
                            self.current_test_index = Some(next_index);
                            if next_index < self.scenarios.len() {
                                println!("Starting test {}", self.scenarios[next_index].name());
                                self.scenarios[next_index].setup(self.dispatch.clone());
                            }

                            ExecutionResult::Processed
                        }
                    }
                }
            }
        }
    }
}
impl IsDisposable for TestsPlugin {
    fn dispose(&mut self) {}
}
impl HasActorAlignment for TestsPlugin {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}

impl IsActor for TestsPlugin {
    fn name(&self) -> String {
        "integration_tests:test_runner".into()
    }
    fn weight(&self) -> u32 {
        256
    }
}
impl ActorsPlugin for TestsPlugin {
    fn apply<T: UseActors>(self, target: T) -> T {
        target.use_actor(Box::new(self))
    }
}

pub struct AdvanceTestsTimerPlugin {
    dispatch: Sender<Message>,
}
impl AdvanceTestsTimerPlugin {
    pub fn new(dispatch: Sender<Message>) -> Self {
        Self { dispatch }
    }
}
impl IsDisposable for AdvanceTestsTimerPlugin {
    fn dispose(&mut self) {}
}
impl IsTickable for AdvanceTestsTimerPlugin {
    fn tick(&mut self, _duration: Duration) {
        _ = self
            .dispatch
            .send(Message::new("integration_tests:advance"));
    }
}
impl IsTimer for AdvanceTestsTimerPlugin {
    fn name(&self) -> String {
        "integration_tests:timer".to_string()
    }

    fn duration(&self) -> std::time::Duration {
        std::time::Duration::from_millis(100)
    }
}
